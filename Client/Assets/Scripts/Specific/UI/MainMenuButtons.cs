﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour
{

    public Button startButton;
    public Button statsButton;
    public Button exitButton;
    public Toggle normal;
    public Toggle hard;

    void Start()
    {
        startButton.onClick.AddListener(OnStartButton);
        statsButton.onClick.AddListener(OnStatsButton);
        exitButton.onClick.AddListener(OnExitButton);
        AssignDifficultyToggles();
    }

    void AssignDifficultyToggles()
    {
        normal.onValueChanged.AddListener((bool value) =>
        {
            if (true)
                GameManager.Instance.selectedDifficulty = Difficulty.Normal;
        });

        hard.onValueChanged.AddListener((bool value) =>
        {
            if (true)
                GameManager.Instance.selectedDifficulty = Difficulty.Hard;
        });
    }

    public void OnStartButton()
    {
        GameManager.Instance.StartGame(true);
    }

    public void OnStatsButton()
    {
        GameManager.Instance.LoadStatsScene();
    }

    public void OnExitButton()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
