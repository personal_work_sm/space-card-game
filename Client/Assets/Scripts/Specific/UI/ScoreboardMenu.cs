﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreboardMenu : MonoBehaviour
{

    public Text[] scoreTopValues;
    public Button backButton;

    // Use this for initialization
    void Start()
    {
        backButton.onClick.AddListener(OnBackButton);

        for (int i = 0; i < scoreTopValues.Length; i++)
        {
            if (i < GameManager.Instance.persistentData.topScores.Length)
            {
                scoreTopValues[i].text = GameManager.Instance.persistentData.topScores[i].ToString();
            }
            else
            {
                scoreTopValues[i].text = "0";
            }
        }
    }


    void OnBackButton()
    {
        GameManager.Instance.LoaMainMenuScene();
    }
}
