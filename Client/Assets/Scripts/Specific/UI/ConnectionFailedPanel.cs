﻿using UnityEngine;
using UnityEngine.UI;

public class ConnectionFailedPanel : MonoBehaviour
{
    public Button retryButton;
    public GameObject container;

    // Use this for initialization
    void Start()
    {
        retryButton.onClick.AddListener(OnRetryConnection);

        if (GameManager.Instance.gameSettingsRequest == RequestStatus.Failed)
        {
            OnRequestFailed();
        }
    }

    private void OnEnable()
    {
        GameManager.requestGameInfoFailed += OnRequestFailed;
    }

    private void OnDisable()
    {
        GameManager.requestGameInfoFailed -= OnRequestFailed;
    }

    void OnRequestFailed()
    {
        container.SetActive(true);
    }

    void OnRetryConnection()
    {
        GameManager.Instance.StartGame(false);
        container.SetActive(false);
    }
}
