﻿using UnityEngine;
using UnityEngine.UI;

public class EndGameMenu : MonoBehaviour
{

    public GameObject container;

    public Text resultText;
    public Button resetButton;
    public Button mainMenuButton;

    // Use this for initialization
    void Start()
    {
        resetButton.onClick.AddListener(OnRestart);
        mainMenuButton.onClick.AddListener(OnMainMenu);
    }

    private void OnEnable()
    {
        GameplayManager.gameWonAction += OnVictory;
        GameplayManager.gameLostAction += OnDefeat;
    }

    private void OnDisable()
    {
        GameplayManager.gameWonAction -= OnVictory;
        GameplayManager.gameLostAction -= OnDefeat;
    }

    void OnVictory()
    {
        resultText.text = "Victory";
        GameManager.Instance.RequestNewConfig();
        container.SetActive(true);
    }

    void OnDefeat()
    {
        resultText.text = "Game Over";
        GameManager.Instance.RequestNewConfig();
        container.SetActive(true);
    }

    public void OnMainMenu()
    {
        GameManager.Instance.LoaMainMenuScene();
    }

    public void OnRestart()
    {
        GameplayManager.Instance.CreateGame();
        container.SetActive(false);
    }
}
