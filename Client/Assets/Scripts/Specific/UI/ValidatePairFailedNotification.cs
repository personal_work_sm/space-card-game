﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidatePairFailedNotification : MonoBehaviour
{

    public RectTransform container;

    Vector3 startPosition;
    Vector3 endPosition;

    private void Start()
    {
        startPosition = container.anchoredPosition;
        endPosition = new Vector2(container.anchoredPosition.x, container.anchoredPosition.y + 50);
    }

    private void OnEnable()
    {
        GameplayManager.pairRequestFailed += ShowNotification;
    }

    private void OnDisable()
    {
        GameplayManager.pairRequestFailed -= ShowNotification;
    }

    [ContextMenu("Show Notification")]
    void ShowNotification()
    {
        StartCoroutine(IShowNotification());
    }

    IEnumerator IShowNotification()
    {
        yield return StartCoroutine(CoMoveObject(container, startPosition, endPosition, 0.3f, 0f));
        yield return StartCoroutine(CoMoveObject(container, endPosition, startPosition, 0.3f, 2f));
    }

    IEnumerator CoMoveObject(RectTransform objTransform, Vector3 startPos, Vector3 endPos, float time, float delay)
    {
        yield return new WaitForSeconds(delay);
        var rate = 1.0f / time;
        var i = 0f;

        while (i < 1.0f)
        {
            i += Time.fixedDeltaTime * rate;
            objTransform.anchoredPosition = Vector3.Lerp(startPos, endPos, i);
            yield return 0;
        }
    }
}
