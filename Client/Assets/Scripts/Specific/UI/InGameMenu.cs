﻿using UnityEngine;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    public GameObject loadingAnimation;

    public Text livesCountText;
    public Text scoreText;
    public Text multiplierText;
    public Text powerUpsLeftText;

    public Button powerUpButton;

    void Start()
    {
        UpdateInfos();
        SetLoadingScreen(false);
        powerUpButton.onClick.AddListener(OnPowerUpButton);
    }

    private void OnEnable()
    {
        GameplayManager.updatePlayerInfoAction += UpdateInfos;
        GameplayManager.loadingAnimationAction += SetLoadingScreen;
    }

    private void OnDisable()
    {
        GameplayManager.updatePlayerInfoAction -= UpdateInfos;
        GameplayManager.loadingAnimationAction -= SetLoadingScreen;
    }

    public void UpdateInfos()
    {
        var playerInfo = GameplayManager.Instance.playerInfo;

        livesCountText.text = playerInfo.livesCount.ToString();
        scoreText.text = "Score: " + playerInfo.gameScore.ToString();
        multiplierText.text = "Multiplier: " + playerInfo.multiplier.ToString();
        powerUpsLeftText.text = "PowerUp: " + playerInfo.powerupsCount.ToString();

        powerUpButton.interactable = playerInfo.powerupsCount > 0;
    }

    public void SetLoadingScreen(bool status)
    {
        loadingAnimation.SetActive(status);
    }

    public void OnPowerUpButton()
    {
        GameplayManager.Instance.hasPowerUpActive = true;
    }
}
