﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public struct GridComponents
{
    public RectTransform rectTransform;
    public GridLayoutGroup grid;

    public GridComponents(GameObject go)
    {
        rectTransform = go.GetComponentWithError<RectTransform>();
        grid = go.GetComponentWithError<GridLayoutGroup>();
    }
}

public class GameCardsGrid : MonoBehaviour
{
    public GameObject gridGO;
    public GameObject cardPrefab;

    GridComponents gridComponents;

    private void Awake()
    {
        gridComponents = new GridComponents(gridGO);
    }

    private void OnEnable()
    {
        GameplayManager.generateGridAction += GenerateMatrix;
    }

    private void OnDisable()
    {
        GameplayManager.generateGridAction -= GenerateMatrix;
    }

    void GenerateMatrix()
    {
        Vector2 cellSize = Vector2.one;
        int rows = GameplayManager.Instance.gameMatrix.GetLength(0);
        int columns = GameplayManager.Instance.gameMatrix.GetLength(1);

        cellSize.x = FilterWidthGrid(gridComponents.rectTransform.rect.width, columns, gridComponents.grid) / columns;
        cellSize.y = FilterHeightGrid(gridComponents.rectTransform.rect.height, rows, gridComponents.grid) / rows;

        gridComponents.grid.constraintCount = columns;
        gridComponents.grid.cellSize = cellSize;

        gridComponents.grid.transform.Clear();

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                GameObject card = Instantiate(cardPrefab);
                card.transform.SetParent(gridComponents.grid.transform);
                card.transform.localScale = Vector3.one;
                card.name = "card " + i + " " + j;

                var cardScript = card.GetComponentWithError<CardScript>();
                GameplayManager.Instance.gameMatrix[i, j].cardScript = cardScript;
                cardScript.cardElement = GameplayManager.Instance.gameMatrix[i, j];
            }
        }
    }

    float FilterWidthGrid(float width, int rows, GridLayoutGroup grid)
    {
        width -= grid.spacing.x * rows;
        width -= grid.padding.left;
        width -= grid.padding.right;
        return width;
    }

    float FilterHeightGrid(float height, int columns, GridLayoutGroup grid)
    {
        height -= grid.spacing.y * columns;
        height -= grid.padding.top;
        height -= grid.padding.bottom;
        return height;
    }

    #region Test Area

    [ContextMenu("Trigger Generation")]
    public void TestGeneration()
    {
        gridComponents = new GridComponents(gridGO);
        Vector2 cellSize = Vector2.one;

        cellSize.x = FilterWidthGrid(gridComponents.rectTransform.rect.width, 3, gridComponents.grid) / 3;
        cellSize.y = FilterHeightGrid(gridComponents.rectTransform.rect.height, 3, gridComponents.grid) / 3;

        gridComponents.grid.constraintCount = 3;
        gridComponents.grid.cellSize = cellSize;

        gridGO.transform.ClearEditor();

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                GameObject card = Instantiate(cardPrefab);
                card.transform.SetParent(gridComponents.grid.transform);
                card.transform.localScale = Vector3.one;
            }
        }
    }

    [ContextMenu("Clear Grid")]
    public void ClearGrid()
    {
        gridComponents.grid.transform.Clear();
    }

    [ContextMenu("Clear Grid Editor")]
    public void ClearGridEditor()
    {
        gridComponents = new GridComponents(gridGO);
        gridComponents.grid.transform.ClearEditor();
    }

    #endregion
}
