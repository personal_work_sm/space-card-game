﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class CardScript : MonoBehaviour
{
    public static Action cardSelected;

    public Button cardButton;
    public Image cardImage;
    public Sprite backgroundSprite;

    public MatrixElement cardElement;

    void Start()
    {
        cardButton.onClick.AddListener(OnCardSelected);
    }

    void OnCardSelected()
    {
        if (GameplayManager.Instance.GetActiveElements() == 2)
            return;

        //Version 2 of the power up
        if (GameplayManager.Instance.hasPowerUpActive)
        {
            BlickCard();
            GameplayManager.Instance.ConsumePowerUp();
            return;
        }

        cardButton.interactable = false;
        cardImage.sprite = cardElement.characterSprite;
        GameplayManager.Instance.CardFliped(cardElement);
        cardSelected.Fire();
    }

    public void ResetCard()
    {
        cardButton.interactable = true;
        cardImage.sprite = backgroundSprite;
    }

    public void BlickCard()
    {
        StartCoroutine(IBlickCard());
    }

    IEnumerator IBlickCard()
    {
        cardImage.sprite = cardElement.characterSprite;
        Vector3 scale = cardImage.transform.localScale;

        for (int i = 0; i < 3; i++)
        {
            yield return StartCoroutine(CoScaleObject(cardImage.transform, scale, scale * 1.1f, 0.3f, 0.05f));
            yield return StartCoroutine(CoScaleObject(cardImage.transform, scale * 1.1f, scale, 0.3f, 0.05f));
        }

        cardImage.sprite = backgroundSprite;
    }

    IEnumerator CoScaleObject(Transform objTransform, Vector3 startScale, Vector3 endScale, float time, float delay)
    {
        yield return new WaitForSeconds(delay);
        var rate = 1.0f / time;
        var i = 0f;

        while (i < 1.0f)
        {
            i += Time.fixedDeltaTime * rate;
            objTransform.localScale = Vector3.Lerp(startScale, endScale, i);
            yield return 0;
        }
    }
}
