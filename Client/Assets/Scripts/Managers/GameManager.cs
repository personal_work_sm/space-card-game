﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public enum ConnectionType
{
    Server,
    Local
}
[System.Serializable]
public class GameInfo
{
    public int cardScore;
    public int multiplier;
    public int livesCount;
    public int powerupsCount;
    public int rowSize;
    public int[] cardIds;

    public static GameInfo CreateFromResponse(string jsonString)
    {
        return JsonUtility.FromJson<GameInfo>(jsonString);
    }
    public GameInfo(int cardScore, int multiplier, int livesCount, int powerupsCount, int rowSize, int[] cardIds)
    {
        this.cardScore = cardScore;
        this.multiplier = multiplier;
        this.livesCount = livesCount;
        this.powerupsCount = powerupsCount;
        this.rowSize = rowSize;
        this.cardIds = cardIds;
    }
}

[System.Serializable]
public class PersistentData
{
    public int[] topScores;

    public PersistentData()
    {
        topScores = new int[5];
    }
}

public enum RequestStatus
{
    None,
    Succesful,
    Failed
}

public enum Difficulty
{
    Normal,
    Hard 
}


public class GameManager : Singleton<GameManager>
{
    public static Action requestGameInfoFailed;

    [Header("Server or Local logic")]
    public ConnectionType configType;
    public ConnectionType validationType;
    public Difficulty selectedDifficulty;

    [Header("Informations variables")]
    public GameInfo gameInfo;
    public PersistentData persistentData;

    [HideInInspector]
    public RequestStatus gameSettingsRequest;
    List<GameInfo> localGameConfigsNormal = new List<GameInfo>();
    List<GameInfo> localGameConfigsHard = new List<GameInfo>();

    #region persistent data variables

    const string folderName = "PersistentData";
    const string fileName = "GameData";
    const string fileExtension = ".dat";

    #endregion

    private void Start()
    {
        InitLocalConfig();
        GetPersistentDataOnStart();
    }

    private void OnApplicationQuit()
    {
        string filePath = GetFilePath();
        SaveData(persistentData, filePath);
    }


    public void StartGame(bool firstTry)
    {
        if (configType == ConnectionType.Local)
        {
            GetLocalConfig();
            LoaGameScene();
        }
        else
        {
            gameSettingsRequest = RequestStatus.None;
            StartCoroutine(GetServerParams(firstTry));
        }
    }

    IEnumerator GetServerParams(bool firstTry)
    {
        if (firstTry)
            LoaWaitingScene();

        UnityWebRequest www;

        if (selectedDifficulty == Difficulty.Normal)
        {
            www = UnityWebRequest.Get("http://localhost:3000/configuration/normal");
        }
        else
        {
            www = UnityWebRequest.Get("http://localhost:3000/configuration/hard");
        }

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            gameSettingsRequest = RequestStatus.Failed;
            requestGameInfoFailed.Fire();
        }
        else
        {
            gameSettingsRequest = RequestStatus.Succesful;
            LoaGameScene();
            gameInfo = GameInfo.CreateFromResponse(www.downloadHandler.text);
        }
    }

    public void RequestNewConfig()
    {
        if (configType == ConnectionType.Local)
        {
            GetLocalConfig();
        }
        else
        {
            gameSettingsRequest = RequestStatus.None;
            StartCoroutine(GetNewConfig());
        }
    }

    IEnumerator GetNewConfig()
    {
        UnityWebRequest www;

        if (selectedDifficulty == Difficulty.Normal)
        {
            www = UnityWebRequest.Get("http://localhost:3000/configuration/normal");
        }
        else
        {
            www = UnityWebRequest.Get("http://localhost:3000/configuration/hard");
        }

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            gameSettingsRequest = RequestStatus.Failed;
        }
        else
        {
            gameSettingsRequest = RequestStatus.Succesful;
            gameInfo = GameInfo.CreateFromResponse(www.downloadHandler.text);
        }
    }

    void GetLocalConfig()
    {
        if(selectedDifficulty == Difficulty.Normal)
        {
            var random = UnityEngine.Random.Range(0, localGameConfigsNormal.Count);
            gameInfo = localGameConfigsNormal[random];
        }else
        {
            var random = UnityEngine.Random.Range(0, localGameConfigsHard.Count);
            gameInfo = localGameConfigsHard[random];
        }
    }

    void InitLocalConfig()
    {
        int[] cardIds;

        cardIds = new int[16] { 2, 7, 1, 8, 4, 3, 4, 1, 5, 6, 2, 7, 3, 5, 6, 8 };

        localGameConfigsNormal.Add(new GameInfo(1, 2, 10, 10, 4, cardIds));

        cardIds = new int[16] { 1, 2, 4, 5, 3, 7, 6, 8, 5, 2, 3, 8, 1, 4, 6, 7 };
        localGameConfigsNormal.Add(new GameInfo(1, 2, 20, 5, 4, cardIds));

        cardIds = new int[36] { 17, 8, 1, 6, 15, 18, 1, 9, 8, 2, 11, 16, 6, 14, 16, 14, 12, 2, 10, 7, 5, 7, 3, 4, 4, 17, 9, 5, 15, 11, 18, 13, 10, 3, 13, 12 };
        localGameConfigsHard.Add(new GameInfo(1, 2, 30, 20, 6, cardIds));

        cardIds = new int[36] { 11, 6, 8, 14, 4, 9, 9, 7, 5, 1, 17, 2, 6, 15, 18, 12, 4, 16, 14, 10, 11, 13, 16, 8, 17, 13, 18, 7, 10, 5, 1, 3, 2, 12, 15, 3 };
        localGameConfigsHard.Add(new GameInfo(1, 2, 50, 10, 6, cardIds));
    }

    #region operations with persisten data

    void CreateData()
    {
        persistentData = new PersistentData();
        string filePath = GetFilePath();

        SaveData(persistentData, filePath);
    }

    void GetPersistentDataOnStart()
    {
        string folderPath = Path.Combine(Application.persistentDataPath, folderName);
        string filePath = GetFilePath();

        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
            CreateData();
            return;
        }

        if (!File.Exists(filePath))
        {
            CreateData();
            return;
        }

        persistentData = LoadData(filePath);
    }

    static void SaveData(PersistentData data, string path)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream fileStream = File.Open(path, FileMode.OpenOrCreate))
        {
            binaryFormatter.Serialize(fileStream, data);
        }
    }

    static PersistentData LoadData(string path)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        try
        {
            using (FileStream fileStream = File.Open(path, FileMode.Open))
            {
                return (PersistentData)binaryFormatter.Deserialize(fileStream);
            }
        }
        catch (Exception error)
        {
            Debug.LogError("Loading file error:" + error);
            File.Delete(path);
            return new PersistentData();
        }
    }

    static string GetFilePath()
    {
        string folderPath = Path.Combine(Application.persistentDataPath, folderName);
        string dataPath = Path.Combine(folderPath, fileName + fileExtension);
        return dataPath;
    }

    #endregion

    #region scene loading functions

    public void LoaMainMenuScene()
    {
        SceneManager.LoadScene(0);
    }

    public void LoaWaitingScene()
    {
        SceneManager.LoadScene(1);
    }

    public void LoaGameScene()
    {
        SceneManager.LoadScene(2);
    }

    public void LoadStatsScene()
    {
        SceneManager.LoadScene(3);
    }
    #endregion

    #region Game Testing

    [ContextMenu("Create Game Settings")]
    public void CreateGameSettingsTest()
    {
        var cardIds = new int[10] { 1, 2, 4, 5, 3, 5, 2, 3, 1, 4 };
        gameInfo = new GameInfo(1, 2, 3, 2, 3, cardIds);
    }

    #endregion
}