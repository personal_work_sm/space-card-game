﻿using UnityEngine;

public enum ManagerType
{
    Global,
    Local
}

public class GeneralManagers : MonoBehaviour
{

    public static GeneralManagers instance;
    public ManagerType managerType;

    public virtual void Awake()
    {
        if (managerType == ManagerType.Global)
        {
            if (!instance)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            instance = this;
        }
    }
}
