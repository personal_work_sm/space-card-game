﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

[System.Serializable]
public struct ActiveCards
{
    public int cardOneId;
    public int cardTwoId;
}

[System.Serializable]
public struct MatrixElement
{
    public int id;
    public CardScript cardScript;
    public Sprite characterSprite;
}

public struct ValidateRequestResponse
{
    public bool isCorrect;
}

[System.Serializable]
public struct PlayerInfo
{
    public int gameScore;
    public int multiplier;
    public int livesCount;
    public int powerupsCount;
    public int cardsMatched;
}

public class GameplayManager : Singleton<GameplayManager>
{
    #region Events Actions

    public static Action generateGridAction;
    public static Action updatePlayerInfoAction;
    public static Action<bool> loadingAnimationAction;
    public static Action gameWonAction;
    public static Action gameLostAction;
    public static Action pairRequestFailed;

    #endregion

    [Header("References variables")]
    public List<Sprite> characterSprites;

    [Header("Informations variables")]
    public PlayerInfo playerInfo;
    public bool hasPowerUpActive;
    public MatrixElement[,] gameMatrix;

    [SerializeField] bool isPairing;
    [SerializeField] ActiveCards activeCards = new ActiveCards();

    List<MatrixElement> activeElements = new List<MatrixElement>();

    private void Start()
    {
        CreateGame();
    }

    public void CreateGame()
    {
        var gameInfo = GameManager.Instance.gameInfo;

        #region set player info

        playerInfo.gameScore = 0;
        playerInfo.cardsMatched = 0;
        playerInfo.livesCount = gameInfo.livesCount;
        playerInfo.multiplier = 1;
        playerInfo.powerupsCount = gameInfo.powerupsCount;

        updatePlayerInfoAction.Fire();

        #endregion

        #region generate game matrix

        var rowsCount = gameInfo.cardIds.Length / gameInfo.rowSize;
        gameMatrix = new MatrixElement[rowsCount, gameInfo.rowSize];
        List<Sprite> sprites = new List<Sprite>(characterSprites);
        Dictionary<int, Sprite> spritesAssigned = new Dictionary<int, Sprite>();

        Sprite auxSprite;
        int auxId;

        for (int i = 0; i < rowsCount; i++)
        {
            for (int j = 0; j < gameInfo.rowSize; j++)
            {
                gameMatrix[i, j] = new MatrixElement();
                auxId = gameInfo.cardIds[i * gameInfo.rowSize + j];
                if (spritesAssigned.ContainsKey(auxId))
                {
                    auxSprite = spritesAssigned[auxId];
                }
                else
                {
                    auxSprite = sprites[UnityEngine.Random.Range(0, sprites.Count)];
                    spritesAssigned.Add(auxId, auxSprite);
                    sprites.Remove(auxSprite);
                }

                gameMatrix[i, j].id = auxId;
                gameMatrix[i, j].characterSprite = auxSprite;
            }
        }

        generateGridAction.Fire();

        #endregion
    }

    public int GetActiveElements()
    {
        return activeElements.Count;
    }

    public void CardFliped(MatrixElement card)
    {

        // Version one of the power up
        //if (hasPowerUpActive)
        //{
        //    hasPowerUpActive = false;
        //    playerInfo.powerupsCount -= 1;
        //    updatePlayerInfoAction.Fire();
        //    return;
        //}

        activeElements.Add(card);

        if (isPairing == false)
        {
            activeCards.cardOneId = card.id;
            isPairing = true;
        }
        else
        {
            activeCards.cardTwoId = card.id;
            isPairing = false;

            if(GameManager.instance.validationType == ConnectionType.Local)
                StartCoroutine(ValidatePairLocal(OnValidationResult));
            else
                StartCoroutine(ValidatePairServer(OnValidationResult));
        }
    }

    void OnValidationResult(bool result)
    {
        var gameInfo = GameManager.Instance.gameInfo;

        if (result)
        {
            playerInfo.gameScore += playerInfo.multiplier * gameInfo.cardScore * 2;
            playerInfo.multiplier = playerInfo.multiplier == 1 ? gameInfo.multiplier : playerInfo.multiplier + gameInfo.multiplier;
            playerInfo.cardsMatched += 2;
        }
        else
        {
            playerInfo.livesCount -= 1;
            playerInfo.multiplier = 1;

            foreach (var element in activeElements)
            {
                element.cardScript.ResetCard();
            }
        }

        if (playerInfo.livesCount == 0)
        {
            CheckForTopScore();
            gameLostAction.Fire();
        }

        if (playerInfo.cardsMatched == gameInfo.cardIds.Length)
        {
            CheckForTopScore();
            gameWonAction.Fire();
        }

        updatePlayerInfoAction.Fire();
        activeElements.Clear();
    }

    IEnumerator ValidatePairLocal(Action<bool> result)
    {
       yield return new WaitForSeconds(0.5f);
       var match = activeCards.cardOneId == activeCards.cardTwoId;
       result.Invoke(match);
    }

    IEnumerator ValidatePairServer(Action<bool> result)
    {
        var body = JsonUtility.ToJson(activeCards);

        UnityWebRequest www = UnityWebRequest.Put("http://localhost:3000/play/", body);
        www.SetRequestHeader("Content-Type", "application/json");

        loadingAnimationAction.Fire(true);

        yield return www.SendWebRequest();

        loadingAnimationAction.Fire(false);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

            foreach (var element in activeElements)
            {
                element.cardScript.ResetCard();
            }
            activeElements.Clear();
            pairRequestFailed.Fire();
        }
        else
        {
            var response = JsonUtility.FromJson<ValidateRequestResponse>(www.downloadHandler.text);
            result.Invoke(response.isCorrect);
        }
    }

    public void CheckForTopScore()
    {
        var scores = GameManager.Instance.persistentData.topScores;

        for (int i = 0; i < scores.Length; i++)
        {
            if (playerInfo.gameScore > scores[i])
            {
                scores[i] = playerInfo.gameScore;
                break;
            }
        }

        GameManager.Instance.persistentData.topScores = scores;
    }

    public void ConsumePowerUp()
    {
        playerInfo.powerupsCount--;
        hasPowerUpActive = false;
        updatePlayerInfoAction.Fire();
    }

    #region Test Area

    [ContextMenu("Make Request")]
    public void TestRequest()
    {
        activeCards.cardOneId = 1;
        activeCards.cardTwoId = 1;
        StartCoroutine(ValidatePairServer(result =>
        {
            Debug.Log("Request result" + result.ToString());
        }));
    }

    [ContextMenu("Create Game")]
    public void CreateGameTest()
    {
        CreateGame();
    }

    [ContextMenu("Add Game Score")]
    public void AddGameScore()
    {
        playerInfo.gameScore = 1900;
        CheckForTopScore();
    }

    #endregion
}
