﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameplayManager))]
public class GameplayManagerInspector : Editor
{
    GameplayManager targetInspector;

    public override void OnInspectorGUI()
    {
        if (targetInspector == null)
        {
            targetInspector = (GameplayManager)target;
        }

        DrawDefaultInspector();

        if (targetInspector.gameMatrix == null)
            return;

        EditorGUILayout.Space();

        var style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter };
        EditorGUILayout.PrefixLabel("Cards Game Matrix", style);

        for (int i = 0; i < targetInspector.gameMatrix.GetLength(0); i++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int j = 0; j < targetInspector.gameMatrix.GetLength(1); j++)
            {
                EditorGUILayout.IntField(targetInspector.gameMatrix[i, j].id);
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
