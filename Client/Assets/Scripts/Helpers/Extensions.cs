﻿using System;
using System.Collections.Generic;
using UnityEngine;


public static class ActionExtensions
{
    public static void Fire(this Action _action)
    {
        if (_action != null)
            _action();
    }

    public static void Fire<T>(this Action<T> _action, T _t)
    {
        if (_action != null)
            _action(_t);
    }

    public static void Fire<T, V>(this Action<T, V> _action, T _t, V _v)
    {
        if (_action != null)
            _action(_t, _v);
    }

    public static void Fire<T, V, U>(this Action<T, V, U> _action, T _t, V _v, U _u)
    {
        if (_action != null)
            _action(_t, _v, _u);
    }

    public static void Fire<T, V, U, W>(this Action<T, V, U, W> _action, T _t, V _v, U _u, W _w)
    {
        if (_action != null)
            _action(_t, _v, _u, _w);
    }
}

public static class ArrayExtensions
{
    public static List<T> ToList<T>(this T[] _array)
    {
        List<T> result = new List<T>();

        for (int i = 0; i < _array.Length; i++)
            result.Add(_array[i]);

        return result;
    }

    public static string ShowContent<T>(this T[] _array)
    {

        string content = "{";

        for (int i = 0; i < _array.Length; i++)
        {

            if (i == _array.Length - 1)
            {
                content += _array[i].ToString() + "}";
            }
            else
            {
                content += _array[i].ToString() + ", ";
            }
        }

        return content;
    }

}

public static class Vector2Extensions
{
    public static Vector2 Negative(this Vector2 _vector)
    {
        return (-1f * _vector);
    }
}

public static class StringExtensions
{
    public static string ToMicroTimeFormat(this float _time)
    {
        var seconds = (int)(_time % 60);
        var miliseconds = (int)((_time * 1000f) % 1000f);

        return string.Format("{0:00} : {1:000}", seconds, miliseconds);
    }

    public static string ToTimeFormat(this float _time)
    {
        var minutes = (int)(_time / 60f);
        var seconds = (int)(_time % 60f);

        return string.Format("{0:00} : {1:00}", minutes, seconds);
    }

    public static bool emptyOrNull(this string _str)
    {
        return string.IsNullOrEmpty(_str);
    }

    public static bool equals(this string _str1, string _str2)
    {
        return string.Equals(_str1, _str2, StringComparison.Ordinal);
    }
}

public static class IntExtensions
{
    public static int? TryIntParse(this string _val)
    {
        int result = 0;

        if (int.TryParse(_val, out result))
        {
            return result;
        }

        return null;
    }
}

public static class FloatExtensions
{
    public static float? TryFloatParse(this string _val)
    {
        float result = 0f;

        if (float.TryParse(_val, out result))
        {
            return result;
        }

        return null;
    }
}

public static class DoubleExtensions
{
    public static double? TryDoubleParse(this string _val)
    {
        double result = 0;

        if (double.TryParse(_val, out result))
        {
            return result;
        }

        return null;
    }
}

public static class TransformExtensions
{
    public static T GetFirstComponentInParents<T>(this Component parent) where T : Component
    {
        Transform t = parent.transform;
        while (t.parent != null)
        {
            T comp = t.parent.GetComponent<T>();
            if (comp != null)
            {
                return comp;
            }
            t = t.parent;
        }
        return default(T); // Could not find a parent with given component.
    }

    public static Transform Clear(this Transform trans)
    {
        var count = trans.childCount;
        for (int i = 0; i < count; i++)
        {
            // use i here because destroy has a delay when executing
            UnityEngine.Object.Destroy(trans.GetChild(i).gameObject);
        }
        return null;
    }

    public static Transform ClearEditor(this Transform trans)
    {
        var count = trans.childCount;
        for (int i = 0; i < count; i++)
        {
            // use 0 here because we have no delay when destrying
            UnityEngine.Object.DestroyImmediate(trans.GetChild(0).gameObject);
        }
        return null;
    }

    public static T GetComponentWithError<T>(this Transform trans)
    {
        var comp = trans.GetComponent<T>();

        if (comp == null) Debug.LogError("Could not find component on Transform " + trans.name);
        return comp;
    }
}

public static class GameObjectExtensions
{
    public static T GetComponentWithError<T>(this GameObject go)
    {
        var comp = go.GetComponent<T>();

        if (comp == null) Debug.LogError("Could not find component on GameObject " + go.name);
        return comp;
    }
}