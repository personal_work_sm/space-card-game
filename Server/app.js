const express = require("express");
const bodyParser = require("body-parser");
const yargs = require("yargs");
const app = express();

const argv = yargs
  .alias("P", "port")
  .help("h")
  .alias("h", "help").argv;

const Sleep = function Sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

const GetRandomNumber = function GetRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

app.use(bodyParser.json());

app.get("/configuration/normal", async (req, res) => {
  const random = GetRandomNumber(0, 2);
  var result;

 switch (random) {
    case 1:
        result = {
            cardScore: 1,
            multiplier: 2,
            livesCount: 10,
            powerupsCount: 10,
            cardIds: [2, 7, 1, 8, 4, 3, 4, 1, 5, 6, 2, 7, 3, 5, 6, 8],
            rowSize: 4
        };
         break;
    default: 
        result = {
            cardScore: 1,
            multiplier: 2,
            livesCount: 20,
            powerupsCount: 5,
            cardIds: [1, 2, 4, 5, 3, 7, 6, 8, 5, 2, 3, 8, 1, 4, 6, 7],
            rowSize: 4
        };
 }

   res.setHeader("Content-Type", "application/json");
  const msToSleep = GetRandomNumber(2 * 1000, 5 * 1000);
  await Sleep(msToSleep);
  res.send(JSON.stringify(result));
  });


 app.get("/configuration/hard", async (req, res) => {
  const random = GetRandomNumber(0, 2);
  var result;

 switch (random) {
    case 1:
        result = {
            cardScore: 1,
            multiplier: 2,
            livesCount: 50,
            powerupsCount: 10,
            cardIds: [17, 8, 1, 6, 15, 18, 1, 9, 8, 2, 11, 16, 6, 14, 16, 14, 12, 2, 10, 7, 5, 7, 3, 4, 4, 17, 9, 5, 15, 11, 18, 13, 10, 3, 13, 12],
            rowSize: 6
        };
         break;
    default: 
       result = {
            cardScore: 1,
            multiplier: 2,
            livesCount: 30,
            powerupsCount: 20,
            cardIds: [11, 6, 8, 14, 4, 9, 9, 7, 5, 1, 17, 2, 6, 15, 18, 12, 4, 16, 14, 10, 11, 13, 16, 8, 17, 13, 18, 7, 10, 5, 1, 3, 2, 12, 15, 3],
            rowSize: 6
        };
 }

  res.setHeader("Content-Type", "application/json");
  const msToSleep = GetRandomNumber(2 * 1000, 5 * 1000);
  await Sleep(msToSleep);
  res.send(JSON.stringify(result));
});


app.put("/play", async (req, res) => {
  const cards = req.body;
  const cardOneId = cards.cardOneId;
  const cardTwoId = cards.cardTwoId;
  const firstIdValid = cardOneId != null;
  const secondIdValid = cardTwoId != null;
  const isCorrect = cardOneId === cardTwoId && cardOneId != null && cardTwoId != null;
  const result = { isCorrect: isCorrect, 
                   firstIdValid: firstIdValid, 
                   secondIdValid : secondIdValid
  };
  res.setHeader("Content-Type", "application/json");
  const msToSleep = GetRandomNumber(2 * 1000, 5 * 1000);
  await Sleep(msToSleep);
  res.send(JSON.stringify(result));
});

const port = argv.P || 3000;

app.listen(port, () => console.log(`Listening on port ${port}!`));

module.exports = app;
