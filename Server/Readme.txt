## Server

You need to install node, url: https://nodejs.org/en/download/

To start the server, you need to execute the following commands in your terminal.

#Install deps
#Expecting to be inside the server folder
npm install
```
#Expecting to be inside the server folder
npm run start
```
This will start a localhost instance on port 3000. If this port is busy, you can change the port just executing:

#Expecting to be inside the server folder
npm run start -- -P=3001
```
Remember to change the port if you started your server with another one in the client code.
